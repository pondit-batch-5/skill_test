class SkillTester {
  String? _questionText;
  bool? _answer;

  SkillTester({String? questionText, bool? answer}) {
    _questionText = questionText;
    _answer = answer;
  }

  bool? get answer => _answer;

  String? get questionText => _questionText;
}
