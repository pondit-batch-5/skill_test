import 'package:flutter/material.dart';
import 'package:skill_test/models/skill_tester.dart';

class SkillMaster {
  int _questionIndex = 0;
  int _questionCount = 0;
  List<Widget> _resultWidgetList = [];
  List<SkillTester> _skillTesterList = [
    SkillTester(
        questionText: "The Battle Of Hastings took place in 1066",
        answer: true),
    //0
    SkillTester(
        questionText: "Only one capital exists in South Africa", answer: false),
    //1
    SkillTester(
        questionText: "Alexander Fleming discovered penicillin", answer: true),
    //2
    SkillTester(questionText: "Madonna’s real name is Madonna", answer: true),
    //3
    SkillTester(questionText: "Ariana Grande is 25 or under", answer: false),
  ];

  String? getQuestionText() {
    return _skillTesterList[_questionIndex].questionText;
  }

  bool? getQuestionAnswerText() {
    return _skillTesterList[_questionIndex].answer;
  }

  void nextQuestion() {
    if (_questionIndex < _skillTesterList.length - 1) {
      // 0<4, 1<4, 2<4, 3<4, 4<4
      _questionIndex = _questionIndex + 1;
      _questionCount = _questionCount + 1;
    } else if (_questionIndex == _skillTesterList.length - 1) {
      _questionCount = _questionCount + 1;
    }
  }

  List<Widget> getResultWidgetList() {
    return _resultWidgetList;
  }

  void addCorrectAnswerWidget() {
    if (_questionCount <= _skillTesterList.length - 1) {
      _resultWidgetList.add(
        Icon(
          Icons.check,
          color: Colors.green,
        ),
      );
    }
  }

  void addWrongAnswerWidget() {
    if (_questionCount <= _skillTesterList.length - 1) {
      _resultWidgetList.add(
        Icon(
          Icons.close,
          color: Colors.red,
        ),
      );
    }
  }
}
