import 'package:flutter/material.dart';
import 'package:skill_test/models/skill_master.dart';

void main() {
  runApp(const SkillTestApp());
}

class SkillTestApp extends StatelessWidget {
  const SkillTestApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  SkillMaster skillMaster = SkillMaster();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              flex: 6,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    skillMaster.getQuestionText() ?? '',
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                    maxLines: 5,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.teal,
                      borderRadius: BorderRadius.circular(12)),
                  child: TextButton(
                    onPressed: () {
                      skillMaster.getQuestionAnswerText()==true? skillMaster.addCorrectAnswerWidget(): skillMaster.addWrongAnswerWidget();

                      // if (skillMaster.getQuestionAnswerText() == true) {
                      //   skillMaster.addCorrectAnswerWidget();
                      // } else {
                      //   skillMaster.addWrongAnswerWidget();
                      // }
                      skillMaster.nextQuestion();
                      setState(() {});
                    },
                    child: Text(
                      'True',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(12)),
                  child: TextButton(
                    onPressed: () {
                      skillMaster.getQuestionAnswerText() == false?skillMaster.addCorrectAnswerWidget(): skillMaster.addWrongAnswerWidget();
                      // if (skillMaster.getQuestionAnswerText() == false) {
                      //   skillMaster.addCorrectAnswerWidget();
                      // } else {
                      //   skillMaster.addWrongAnswerWidget();
                      // }
                      skillMaster.nextQuestion();
                      setState(() {});
                    },
                    child: Text(
                      'False',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                children: skillMaster.getResultWidgetList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
